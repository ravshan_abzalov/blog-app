class Comment < ActiveRecord::Base
	belongs_to :article
	validates :name, presence: true
	validates :body, presence: true
	validates :email, presence: true
	validate :article_should_be_published
	after_create :send_email_comment

	def article_should_be_published
		errors.add(:article_id, " is not published yet") if 
		article && !article.published?
	end

	def email_article_author
		puts "We will notify #{article.user.email} in Chapter 9."
	end

	def send_email_comment
		Notifier.comment_added(self).deliver_now
	end

end
